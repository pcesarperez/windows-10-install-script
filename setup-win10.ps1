# Shows a standard message in base color, with no new line
#
# @param $Message Message to render.
Function Show-Message {
    [CmdletBinding()]
    param (
        [Parameter()]
        [string]$Message
    )

    Write-Host $Message -ForegroundColor Cyan -NoNewLine
}


# Shows a message in two different colors, using one for standard text and another for important text, with no new line.
# The important text should be surrounded by a pair of curly braces.
# For example: "This is standard, but {this one} is important, though.".
#
# @param $Message Message to render in two colors.
Function Show-HighlightedMessage {
    [CmdletBinding()]
    param (
        [Parameter()]
        [string]$Message
    )

    $OpenDelimiter = $Message.IndexOf("{")
    $EndDelimiter = $Message.LastIndexOf("}")

    $StartMessage = $Message.Substring(0, $OpenDelimiter)
    $MiddleMessage = $Message.Substring($OpenDelimiter + 1, $EndDelimiter - $OpenDelimiter - 1)
    $EndMessage = $Message.Substring($EndDelimiter + 1, $Message.Length - $EndDelimiter - 1)

    Write-Host $StartMessage -ForegroundColor Cyan -NoNewLine
    Write-Host $MiddleMessage -ForegroundColor Yellow -NoNewLine
    Write-Host $EndMessage -ForegroundColor Cyan -NoNewLine
}


# Shows a confirmation mark, pointing out that the previous process has been successful.
Function Show-OK {
    Write-Host "[OK]" -ForegroundColor Green
}


# Shows a failure mark, pointing out that the previous process has failed.
Function Show-NO {
    Write-Host "[NO]" -ForegroundColor Red
}


# First off, let's disable transparency effects.
Show-Message -Message "Disabling transparency effects..."
Set-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize" -Name EnableTransparency -Value 0
Show-OK

# Set the dark theme, so that our eyes don't bleed out.
Show-Message -Message "Setting dark theme..."
Set-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize" -Name AppsUseLightTheme -Value 0
Show-OK

# Let's get rid of the standard background, with a plain black background instead. This is serious business.
Show-Message -Message "Setting black background..."
Set-ItemProperty -Path "HKCU:\Control Panel\Desktop" -Name WallPaper -Value ""
Set-ItemProperty -Path "HKCU:\Control Panel\Colors" -Name Background -Value "0 0 0"
Show-OK

# Show the filename extensions. Why is this enabled by default is beyond me.
Show-Message -Message "Disabling the hiding of filename extensions..."
Set-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name HideFileExt -Value 0
Show-OK

# Resets Windows Explorer for the changes to take place.
Show-Message -Message "Restarting Windows Explorer..."
Stop-Process -ProcessName explorer
Show-OK
